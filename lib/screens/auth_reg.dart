
import 'package:snt_delegate/services/RootService.dart';
import 'package:flutter/material.dart';
import 'package:snt_delegate/widgets/NewUserBody.dart';

class AuthRegScreen extends StatefulWidget {
  @override
  _AuthRegScreenState createState() => _AuthRegScreenState();
}

class _AuthRegScreenState extends State<AuthRegScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: NewUserBody(
      onSubmit: doSubmit,
    ));
  }

  doSubmit(_cntrls, _userStreet, userSnt) {
    RootService().user.model.name = _cntrls['username'].text;
    RootService().user.model.lastPayedCounterBeforeRegisterDay =
        _cntrls['lastPayedCounterBeforeRegisterDay'].text;
    RootService().user.model.lastPayedCounterBeforeRegisterNight =
        _cntrls['lastPayedCounterBeforeRegisterNight'].text;
    RootService().user.model.currentCounterDay =
        _cntrls['currentCounterDay'].text;
    RootService().user.model.currentCounterNight =
        _cntrls['currentCounterNight'].text;
    RootService().user.model.counter = _cntrls['counter'].text;
    RootService().user.model.counterType =
        int.parse(_cntrls['counter_type'].text);
    RootService().user.model.street = _userStreet;
    RootService().user.model.snt = userSnt;
    RootService().user.create().then(redirect);
  }

  redirect(v) {
    Navigator.of(context).pushReplacementNamed("/menu/");
  }
}
