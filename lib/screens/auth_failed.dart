import 'package:flutter/material.dart';

class AuthFailedScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AuthFailedScreenBody(),
    );
  }
}

class AuthFailedScreenBody extends StatefulWidget {
  @override
  _AuthFailedScreenBodyState createState() => _AuthFailedScreenBodyState();
}

class _AuthFailedScreenBodyState extends State<AuthFailedScreenBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.error,
            color: Colors.red,
          ),
          Text("Авторизация не пройдена")
        ],
      ),
    );
  }
}
