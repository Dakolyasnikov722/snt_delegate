import 'dart:math';

import 'package:flutter/material.dart';
import 'package:snt_delegate/models/SntModel.dart';
import 'package:snt_delegate/models/UserModel.dart';
import 'package:snt_delegate/services/RootService.dart';
import 'package:snt_delegate/widgets/NewUserBody.dart';

class NewUserScreen extends StatefulWidget {
  @override
  _NewUserScreenState createState() => _NewUserScreenState();
}

class _NewUserScreenState extends State<NewUserScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Новый пользователь'),
      ),
      body: NewUserBody(
        onSubmit: doSubmit,
      ),
    );
  }

  doSubmit(_cntrls, street, SntModel snt) {
    UserModel foreignUser = UserModel();
    foreignUser.name = _cntrls['username'].text;
    foreignUser.lastPayedCounterBeforeRegisterDay =
        _cntrls['lastPayedCounterBeforeRegisterDay'].text;
    foreignUser.lastPayedCounterBeforeRegisterNight =
        _cntrls['lastPayedCounterBeforeRegisterNight'].text;
    foreignUser.currentCounterDay = _cntrls['currentCounterDay'].text;
    foreignUser.currentCounterNight = _cntrls['currentCounterNight'].text;
    foreignUser.counter = _cntrls['counter'].text;
    foreignUser.counterType = int.parse(_cntrls['counter_type'].text);
    foreignUser.street = street;
    foreignUser.snt = snt;
    String randomUid = DateTime.now().microsecondsSinceEpoch.toString() +
        Random().nextInt(1000).toString();
    RootService().user.regForeignUser(randomUid, foreignUser).then(redirect);
  }

  redirect(v) {
    Navigator.of(context).pushReplacementNamed("/menu/");
  }
}
