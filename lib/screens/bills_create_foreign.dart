
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:snt_delegate/models/BillModel.dart';
import 'package:snt_delegate/services/RootService.dart';
import 'package:snt_delegate/widgets/ImageChooser.dart';
import 'package:flutter/material.dart';

class BillCreateForeignScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Подача показаний за другого'),
      ),
      body: BillCreateForeignBody(),
    );
  }
}

class BillCreateForeignBody extends StatefulWidget {
  @override
  _BillCreateForeignBodyState createState() => _BillCreateForeignBodyState();
}

class _BillCreateForeignBodyState extends State<BillCreateForeignBody> {
  BillModel bill = BillModel();
  TextEditingController dayCount = TextEditingController();
  TextEditingController nightCount = TextEditingController();
  TextEditingController billCreatorId = TextEditingController();
  TextEditingController billCreatorName = TextEditingController();
  ImageChooserController devicePhoto = ImageChooserController(null);
  ImageChooserController checkPhoto = ImageChooserController(null);
  String ownerId;
  bool isPayed = false;

  bool isLoading = true;

  Map voters;

  @override
  void initState() {
    super.initState();
    load();
  }

  load() async {
    var doc = await Firestore.instance
        .collection('election')
        .document(RootService().user.uid)
        .get();
    setState(() {
      voters = doc['users'];
      if(voters != null){
        ownerId = voters.keys.toList()[0];
      }
      isLoading = false;
    });
  }

  doPayChange(bool value) {
    setState(() {
      isPayed = value;
    });
  }

  bool inLoading = false;
  validate() {
    if (dayCount.text.length < 3) {
      return false;
    }
    if (nightCount.text.length < 3) {
      return false;
    }
    return true;
  }

  startSubmit() {
    if (validate()) {
      setState(() => inLoading = true);
      submit();
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          'Проверьте введеные значения',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.red,
      ));
    }
  }

  submit() async {
    await doSubmit();
    doneSubmit();
  }

  doneSubmit() {
    setState(() => inLoading = false);
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(
        'Данные отправлены',
        style: TextStyle(color: Colors.white),
      ),
      backgroundColor: Colors.green,
    ));
  }

  doSubmit() async {
    BillModel bill = BillModel();
    bill.dayCount = dayCount.text;
    bill.nightCount = nightCount.text;
    bill.billOwnerId = ownerId;
    bill.billOwnerName = voters[ownerId]['name'];
    bill.billUserId = RootService().user.uid;
    bill.billCreatorName = RootService().user.model.name;
    if (devicePhoto.value != null) {
      var uploadDevicePhoto = await FirebaseStorage()
          .ref()
          .getRoot()
          .child(Timestamp.now().millisecondsSinceEpoch.toString())
          .putFile(devicePhoto.value)
          .onComplete;
      bill.devicePhoto = await uploadDevicePhoto.ref.getPath();
    }
    if (checkPhoto.value != null) {
      var uploadCheckPhoto = await FirebaseStorage()
          .ref()
          .getRoot()
          .child(Timestamp.now().millisecondsSinceEpoch.toString())
          .putFile(checkPhoto.value)
          .onComplete;
      bill.checkPhoto = await uploadCheckPhoto.ref.getPath();
    }

    bill.creationTime = Timestamp.now();
    bill.isPayed = isPayed;
    await bill.save();
  }

  Widget buildLoader() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading == true) {
      return buildLoader();
    }
    if(voters == null){
      return Center(child: Text("Нет пользователей"),);
    }
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          DropdownButton(
            disabledHint: Text('За кого передаете показания'),
            hint: Text('За кого передаете показания'),
            isExpanded: true,
            onChanged: (v) {
              setState(() {
                ownerId = v;
              });
            },
            value: ownerId,
            items: List.generate(voters.length, (i) {
              var uid = voters.keys.toList()[i];
              var voter = voters[uid];
              return DropdownMenuItem(
                child: Container(
                  child: Text(voter['name']),
                ),
                value: voter['uid'],
              );
            }),
          ),
          TextField(
            keyboardType: TextInputType.phone,
            decoration: InputDecoration(
                labelText: "Показания дня", hintText: "Показания дня"),
            controller: dayCount,
          ),
          TextField(
            keyboardType: TextInputType.phone,
            decoration: InputDecoration(
                labelText: "Показания ночи", hintText: "Показания ночи"),
            controller: nightCount,
          ),
          ImageChooser(
            title: "Фото счетчика",
            controller: devicePhoto,
          ),
          ImageChooser(
            title: "Фото квитанции",
            controller: checkPhoto,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Оплачено ли'),
              Switch(
                value: isPayed,
                onChanged: doPayChange,
              )
            ],
          ),
          Flexible(
            child: Container(),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: RaisedButton(
              color: Colors.blue,
              textColor: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                    inLoading ? CircularProgressIndicator() : Text('Отправить'),
              ),
              onPressed: inLoading ? null : startSubmit,
            ),
          )
        ],
      ),
    );
  }
}
