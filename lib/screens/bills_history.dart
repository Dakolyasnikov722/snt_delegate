import 'package:snt_delegate/models/BillModel.dart';
import 'package:snt_delegate/services/RootService.dart';
import 'package:flutter/material.dart';

class BillHistoryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("История показаний"),
      ),
      body: BillHistoryBody(),
    );
  }
}

class BillHistoryBody extends StatefulWidget {
  @override
  _BillHistoryBodyState createState() => _BillHistoryBodyState();
}

class _BillHistoryBodyState extends State<BillHistoryBody> {
  bool isLoading = true;
  List<BillModel> bills;
  load() async {
    RootService().bills.getHistory().then((billsHistory) {
      setState(() {
        bills = billsHistory;
        isLoading = false;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    load();
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading || bills == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return Container(
        child: ListView(
      children: List.generate(bills.length, (i) {
        var bill = bills[i];
        return ListTile(
          leading: Icon(
            bill.isPayed == true ? Icons.check : Icons.error,
            color: bill.isPayed == true ? Colors.green : Colors.red,
          ),
          title: Text(bill.creationTime.toDate().toUtc().toString()),
          subtitle:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text("${bill.dayCount}/${bill.nightCount}"),
            Text(bill.isPayed == true ? "Оплачен" : "Не оплачен"),
          ]),
          isThreeLine: true,
        );
      }),
    ));
  }
}
