import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:snt_delegate/models/KontragentModel.dart';
import 'package:snt_delegate/models/SntModel.dart';
import 'package:snt_delegate/models/StreetModel.dart';
import 'package:snt_delegate/services/RootService.dart';
import 'package:snt_delegate/widgets/CounterChooser.dart';
import 'package:snt_delegate/widgets/KontragentChooser.dart';
import 'package:snt_delegate/widgets/SntChooser.dart';
import 'package:snt_delegate/widgets/StreetChooser.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Мой профиль'),
      ),
      body: ProfileScreenBody(),
    );
  }
}

class ProfileScreenBody extends StatefulWidget {
  @override
  _ProfileScreenBodyState createState() => _ProfileScreenBodyState();
}

class _ProfileScreenBodyState extends State<ProfileScreenBody> {
  bool isLoading = true;

  PageController _pageController = PageController();

  List<StreetModel> streets;
  List<Map> counters;

  StreetModel _userStreet = RootService().user.model.street;
  SntModel _userSnt = RootService().user.model.snt;
  KontragentModel _userElectrocityKontrakent =
      RootService().user.model.electrokontragent;

  Map<String, TextEditingController> _cntrls = {
    "name": TextEditingController(text: RootService().user.model.name),
    "lastname": TextEditingController(text: RootService().user.model.lastname),
    "grandname":
        TextEditingController(text: RootService().user.model.grandname),
    "house": TextEditingController(text: RootService().user.model.house),
    "phone": TextEditingController(text: RootService().user.model.phone),
    "counter": TextEditingController(text: RootService().user.model.counter),
    "counter_type": TextEditingController(
        text: RootService().user.model.counterType.toString()),
    "counterModel":
        TextEditingController(text: RootService().user.model.counterModel),
    "counterSN":
        TextEditingController(text: RootService().user.model.counterSN),
    "counterSetupDate":
        TextEditingController(text: RootService().user.model.counterSetupDate),
    "uzoAmpers":
        TextEditingController(text: RootService().user.model.uzoAmpers),
    "mukAccountId":
        TextEditingController(text: RootService().user.model.mukAccountId),
    "mukPassword":
        TextEditingController(text: RootService().user.model.mukPassword),
    "humanCount":
        TextEditingController(text: RootService().user.model.humanCount),
  };

  @override
  void initState() {
    super.initState();
    loadData().then((v) {
      setState(() {
        isLoading = false;
      });
    });
  }

  loadData() async {
    streets = RootService().streetStore.streets;
    var rawCounters = await Firestore.instance
        .collection('settings')
        .document('countertypes')
        .get();
    counters = List<Map>.generate(rawCounters.data.length, (i) {
      return rawCounters.data[rawCounters.data.keys.toList()[i]];
    });
  }

  doSubmit() async {
    RootService().user.model.name = _cntrls['name'].text;
    RootService().user.model.lastname = _cntrls['lastname'].text;
    RootService().user.model.grandname = _cntrls['grandname'].text;
    RootService().user.model.phone = _cntrls['phone'].text;

    RootService().user.model.snt = _userSnt;
    RootService().user.model.street = _userStreet;
    RootService().user.model.house = _cntrls['house'].text;

    RootService().user.model.counter = _cntrls['counter'].text;
    RootService().user.model.counterModel = _cntrls['counterModel'].text;
    RootService().user.model.counterSN = _cntrls['counterSN'].text;

    RootService().user.model.mukAccountId = _cntrls['mukAccountId'].text;
    RootService().user.model.mukPassword = _cntrls['mukPassword'].text;
    RootService().user.model.humanCount = _cntrls['humanCount'].text;

    RootService().user.model.counterSetupDate =
        _cntrls['counterSetupDate'].text;
    RootService().user.model.uzoAmpers = _cntrls['uzoAmpers'].text;
    RootService().user.model.counterType =
        _cntrls['counter_type'].text == 'null'
            ? null
            : int.parse(_cntrls['counter_type'].text);
    RootService().user.model.electrokontragent =
        this._userElectrocityKontrakent;
    RootService().user.update().then(showResult);
  }

  showResult(v) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(
        "Сохранено",
        style: TextStyle(color: Colors.white),
      ),
      backgroundColor: Colors.green,
    ));
  }

  changeDelegate(bool isDelegate) {
    setState(() {
      RootService().user.model.isDelegate = isDelegate;
    });
  }

  Widget buildLoader() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  onChangeStreet(StreetModel street) {
    _userStreet = street;
  }

  onChangeCounter(Map counter) {
    _cntrls['counter'].text = counter['title'];
    _cntrls['counter_type'].text = counter['value'].toString();
  }

  chooseSnt() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (ctx) => SntChooserScreen(
                  onChanged: onChangeSnt,
                  snt: RootService().sntStore.snts,
                )));
  }

  chooseElectro() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (ctx) => KontragetChooserScreen(
                  onChanged: onChangeElectro,
                  list: RootService().kontragentService.electricity,
                )));
  }

  onChangeElectro(KontragentModel model) {
    setState(() {
      _userElectrocityKontrakent = model;
    });
  }

  onChangeSnt(SntModel snt) {
    setState(() {
      _userSnt = snt;
    });
  }

  chooseStreet() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (ctx) => StreetChooserScreen(
                  onChanged: onChangeStreet,
                  streets: streets,
                )));
  }

  chooseCounter() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (ctx) => CounterChooserScreen(
                  onChanged: onChangeCounter,
                  counters: counters,
                )));
  }

  nextPage() {
    setState(() {});
    doSubmit();
    _pageController.nextPage(
        curve: Curves.bounceInOut, duration: Duration(milliseconds: 300));
  }

  prevPage() {
    setState(() {});
    doSubmit();
    _pageController.previousPage(
        curve: Curves.bounceInOut, duration: Duration(milliseconds: 300));
  }

  bool isValid() {
    if (_cntrls['name'].text.length < 2) {
      return false;
    }
    if (_cntrls['lastname'].text.length < 2) {
      return false;
    }
    if (_cntrls['grandname'].text.length < 2) {
      return false;
    }
    if (_cntrls['phone'].text.length < 2) {
      return false;
    }
    if (_cntrls['counter_type'].text.length < 1) {
      return false;
    }
    if (_cntrls['counter_type'].text.length < 1) {
      return false;
    }
    if (_cntrls['house'].text.length < 1) {
      return false;
    }
    if (RootService().user.model.snt == null) {
      return false;
    }
    if (RootService().user.model.electrokontragent == null) {
      return false;
    }

    return true;
  }

  Widget buildPage({List<Widget> children, bool isLast = false}) {
    List<Widget> items = children;
    items.addAll(<Widget>[
      Padding(
        padding: const EdgeInsets.only(top: 25),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            RaisedButton.icon(
              textColor: Colors.white,
              onPressed: prevPage,
              icon: Icon(
                Icons.chevron_left,
                color: Colors.white,
              ),
              label: Text("Назад"),
              color: Colors.blue,
            ),
            isValid()
                ? Text(
                    "Все заполнено",
                    style: TextStyle(color: Colors.green),
                  )
                : Text(
                    "Не все заполнено",
                    style: TextStyle(color: Colors.red),
                  ),
            RaisedButton.icon(
              textColor: Colors.white,
              onPressed: isLast ? doSubmit : nextPage,
              icon: Icon(
                isLast ? Icons.save : Icons.chevron_right,
                color: Colors.white,
              ),
              label: Text(isLast ? "Готово" : "Дальше"),
              color: Colors.green,
            ),
          ],
        ),
      )
    ]);
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
      ),
      child: ListView(children: items),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return buildLoader();
    }
    return PageView(
      controller: _pageController,
      children: <Widget>[
        buildPage(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 25),
            ),
            TextField(
              decoration: InputDecoration(
                  labelText: "Имя", border: OutlineInputBorder()),
              controller: _cntrls['name'],
            ),
            Padding(
              padding: EdgeInsets.only(top: 25),
            ),
            TextField(
              decoration: InputDecoration(
                  labelText: "Фамилия", border: OutlineInputBorder()),
              controller: _cntrls['lastname'],
            ),
            Padding(
              padding: EdgeInsets.only(top: 25),
            ),
            TextField(
              decoration: InputDecoration(
                  labelText: "Отчество", border: OutlineInputBorder()),
              controller: _cntrls['grandname'],
            ),
            Padding(
              padding: EdgeInsets.only(top: 25),
            ),
            TextField(
              inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  labelText: "Телефон", border: OutlineInputBorder()),
              controller: _cntrls['phone'],
            ),
          ],
        ),
        buildPage(children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('У Вас тариф ДЕНЬ/НОЧЬ?'),
          ),
          RaisedButton(
            child: Text("Да"),
            onPressed: () {
              onChangeCounter({'title': "Двухтарифный", 'value': 2});
              nextPage();
            },
          ),
          RaisedButton(
            child: Text("Нет"),
            onPressed: () {
              onChangeCounter({'title': "Однотарифный", 'value': 1});
              nextPage();
            },
          )
        ]),
        buildPage(children: [
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          Text("Ваш СНТ Железнодорожник ?"),
          RaisedButton(
            child: Text("Да"),
            onPressed: () {
              onChangeSnt(SntModel(index: 0, title: "Железнодорожник"));
              nextPage();
            },
          ),
          RaisedButton(
            child: Text("Нет"),
            onPressed: () {
              chooseSnt();
            },
          )
        ]),
        buildPage(children: [
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          Text("Ваша улица"),
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          Text(
            _userStreet.title,
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          RaisedButton(
            child: Text('Выбрать улицу'),
            onPressed: chooseStreet,
          ),
        ]),
        buildPage(children: [
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          Text("Ваш дом"),
          Padding(
            padding: EdgeInsets.only(top: 25),
          ),
          TextField(
            decoration: InputDecoration(
                labelText: "Номер дома", border: OutlineInputBorder()),
            controller: _cntrls['house'],
          ),
        ]),
        buildPage(children: [
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          Text(
              'Вы оплачиваете электроэнергию на расчетный счет СНТ "Железнодорожник" (по договору общему 10177)?'),
          RaisedButton(
            child: Text("Да"),
            onPressed: () {
              onChangeElectro(RootService().kontragentService.electricity[0]);
              nextPage();
            },
          ),
          RaisedButton(
            child: Text("Нет"),
            onPressed: () {
              chooseElectro();
            },
          )
        ]),
        buildPage(children: [
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          TextField(
            decoration: InputDecoration(
                labelText: "Лицевой счет МУК", border: OutlineInputBorder()),
            controller: _cntrls['mukAccountId'],
          ),
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          TextField(
            decoration: InputDecoration(
                labelText: "Пароль МУК", border: OutlineInputBorder()),
            controller: _cntrls['mukPassword'],
          ),
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          TextField(
            inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                labelText: "Количество постоянно проживающих",
                border: OutlineInputBorder()),
            controller: _cntrls['humanCount'],
          ),
        ]),
        buildPage(children: [
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          Text("Ваш счетчик"),
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          TextField(
            decoration: InputDecoration(
                labelText: "Модель счетчика", border: OutlineInputBorder()),
            controller: _cntrls['counterModel'],
          ),
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          TextField(
            decoration: InputDecoration(
                labelText: "Серийный номер счетчика",
                border: OutlineInputBorder()),
            controller: _cntrls['counterSN'],
          ),
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          TextField(
            decoration: InputDecoration(
                labelText: "Дата установки", border: OutlineInputBorder()),
            controller: _cntrls['counterSetupDate'],
          ),
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          TextField(
            inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                labelText: "Вводной автомат (А)", border: OutlineInputBorder()),
            controller: _cntrls['uzoAmpers'],
          ),
        ]),
        buildPage(isLast: true, children: [
          CheckboxListTile(
            value: RootService().user.model.isDelegate,
            onChanged: changeDelegate,
            title: Text('Хочу быть делегатом'),
          ),
        ])
      ],
    );
  }
}
