import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:snt_delegate/services/RootService.dart';
import 'package:snt_delegate/widgets/UserRating.dart';
import 'package:flutter/material.dart';

class ElectionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Голосование'),
      ),
      body: ElectionBody(),
    );
  }
}

class ElectionBody extends StatefulWidget {
  @override
  _ElectionBodyState createState() => _ElectionBodyState();
}

class _ElectionBodyState extends State<ElectionBody> {
  @override
  void initState() {
    super.initState();
    load();
  }

  List<DocumentSnapshot> delegats;
  load() async {
    var snapshot =
        await Firestore.instance.collection('election').getDocuments();
    setState(() {
      delegats = snapshot.documents;
    });
  }

  Widget buildLoader() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  startLoading() {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(
        'Идет отправка',
        style: TextStyle(color: Colors.white),
      ),
      duration: Duration(days: 2),
      backgroundColor: Colors.blue,
    ));
  }

  stopLoading() {
    Scaffold.of(context).hideCurrentSnackBar();
  }

  void doChoose(String delegateId) async {
    startLoading();
    RootService().user.chooseMyDelegate(delegateId, choice: true).then((v) {
      load().then((v) {
        stopLoading();
      });
    });
  }

  void notChoose(String delegateId) async {
    startLoading();
    RootService().user.chooseMyDelegate(delegateId, choice: false).then((v) {
      load().then((v) {
        stopLoading();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (delegats == null) {
      return buildLoader();
    }
    if (delegats.length == 0) {
      return Center(
        child: Text("Делегатов ещё нет "),
      );
    }
    return ListView(
        children: List.generate(delegats.length, (i) {
      var delegate = delegats[i];
      var delegateId = delegats[i].documentID;

      bool isUserChoosed = delegate['users'] != null &&
          delegate['users'].length > 0 &&
          delegate['users'].keys.contains(RootService().user.uid);
      if (delegate['active'] != true) {
        return Container();
      }
      return Card(
        elevation: 2,
        child: Container(
          child: Column(children: [
            ListTile(
              title: Text(delegate['name']),
              subtitle: Text(delegate['street'] == null ? "":delegate['street']),
              trailing: UserRating(
                rating: ['1', '2', '3'],
              ),
            ),
            ButtonBar(
              children: <Widget>[
                RaisedButton(
                  color: !isUserChoosed ? Colors.blueAccent : Colors.red,
                  child: Text(
                    !isUserChoosed ? 'Выбрать' : "Отказаться",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () => !isUserChoosed
                      ? doChoose(delegateId)
                      : notChoose(delegateId),
                )
              ],
            )
          ]),
        ),
      );
    }));
  }
}
