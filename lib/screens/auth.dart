import 'package:permissions_plugin/permissions_plugin.dart';
import 'package:snt_delegate/services/RootService.dart';
import 'package:flutter/material.dart';

class AuthScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AuthScreenBody(),
    );
  }
}

class AuthScreenBody extends StatefulWidget {
  @override
  _AuthScreenBodyState createState() => _AuthScreenBodyState();
}

class _AuthScreenBodyState extends State<AuthScreenBody> {
  @override
  void initState() {
    super.initState();

    RootService().init().then(tryAuth);
  }

  tryAuth(v) async {
    RootService().user.tryAuth(context);
    PermissionsPlugin.requestPermissions([Permission.WRITE_EXTERNAL_STORAGE]);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
