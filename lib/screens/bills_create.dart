import 'dart:io';
import 'dart:math';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

import 'package:qr_flutter/qr_flutter.dart';
import 'package:share_extend/share_extend.dart';

import 'package:snt_delegate/models/BillModel.dart';
import 'package:snt_delegate/services/RootService.dart';
import 'package:snt_delegate/widgets/CounterDataWidget.dart';
import 'package:snt_delegate/widgets/ImageChooser.dart';
import 'package:flutter/material.dart';

class BillCreateScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Подача показаний'),
      ),
      body: BillCreateBody(),
    );
  }
}

class BillCreateBody extends StatefulWidget {
  // String dayCount;
  // String nightCount;
  // bool isPayed;
  // String billCreatorId;
  // String billCreatorName;
  // DocumentReference devicePhoto;
  // DocumentReference checkPhoto;
  // Timestamp creationTime;
  @override
  _BillCreateBodyState createState() => _BillCreateBodyState();
}

class _BillCreateBodyState extends State<BillCreateBody> {
  BillModel bill = BillModel();
  CounterDataController dayCount = CounterDataController(CounterData(count: 6));
  CounterDataController nightCount =
      CounterDataController(CounterData(count: 6));
  TextEditingController billCreatorId = TextEditingController();
  TextEditingController billCreatorName = TextEditingController();
  ImageChooserController devicePhoto = ImageChooserController(null);
  ImageChooserController checkPhoto = ImageChooserController(null);
  bool isPayed = false;
  Map lastBill;

  doPayChange(bool value) {
    setState(() {
      isPayed = value;
    });
  }

  bool inLoading = false;
  bool isDataLoaded = false;

  @override
  void initState() {
    super.initState();
    loadLastBill();
  }

  loadLastBill() async {
    var data = await Firestore.instance
        .collection('bills')
        .where('billOwnerId', isEqualTo: RootService().user.uid)
        .orderBy('creationTime', descending: true)
        .limit(1)
        .getDocuments();
    if (data.documents != null && data.documents.length > 0) {
      lastBill = data.documents.last.data;
      dayCount.text = lastBill['dayCount'];
      nightCount.text = lastBill['nightCount'];
    } else {}
    setState(() => isDataLoaded = true);
  }

  validate() {
    if (lastBill != null) {
      if (int.parse(dayCount.text) - 1000 > int.parse(lastBill['dayCount'])) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Показания дня слишком большие'),
        ));
        return false;
      }
      if (int.parse(nightCount.text) - 1000 >
          int.parse(lastBill['nightCount'])) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Показания ночи слишком большие'),
        ));
        return false;
      }
    }
    if (dayCount.text.length < 3) {
      return false;
    }
    if (nightCount.text.length < 3) {
      return false;
    }
    return true;
  }

  startSubmit() {
    if (validate()) {
      setState(() => inLoading = true);
      submit();
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(
          'Проверьте введеные значения',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.red,
      ));
    }
  }

  submit() async {
    await doSubmit();
    doneSubmit();
  }

  doneSubmit() {
    setState(() => inLoading = false);
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(
        'Данные отправлены',
        style: TextStyle(color: Colors.white),
      ),
      backgroundColor: Colors.green,
    ));
  }

  doSubmit() async {
    BillModel bill = BillModel();
    bill.dayCount = dayCount.text;
    bill.nightCount = nightCount.text;
    bill.billOwnerId = RootService().user.uid;
    bill.billOwnerName = RootService().user.model.name;
    bill.billUserId = RootService().user.uid;
    bill.billCreatorName = RootService().user.model.name;
    if (devicePhoto.value != null) {
      var uploadDevicePhoto = await FirebaseStorage()
          .ref()
          .getRoot()
          .child(Timestamp.now().millisecondsSinceEpoch.toString())
          .putFile(devicePhoto.value)
          .onComplete;
      bill.devicePhoto = await uploadDevicePhoto.ref.getPath();
    }
    if (checkPhoto.value != null) {
      var uploadCheckPhoto = await FirebaseStorage()
          .ref()
          .getRoot()
          .child(Timestamp.now().millisecondsSinceEpoch.toString())
          .putFile(checkPhoto.value)
          .onComplete;
      bill.checkPhoto = await uploadCheckPhoto.ref.getPath();
    }

    bill.creationTime = Timestamp.now();
    bill.isPayed = isPayed;
    await bill.save();
  }

  nextPage() {
    if (_pageController.page == 0.0) {
      getQr();
    }
    _pageController.nextPage(
        curve: Curves.bounceInOut, duration: Duration(milliseconds: 300));
  }

  prevPage() {
    _pageController.previousPage(
        curve: Curves.bounceInOut, duration: Duration(milliseconds: 300));
    setState(() {});
  }

  Widget buildPage({List<Widget> children, bool isLast = false}) {
    List<Widget> items = List();
    items.add(Padding(
      padding: EdgeInsets.only(top: 25),
    ));
    items.addAll(children);
    items.addAll([
      Flexible(
        child: Container(),
      ),
      Padding(
        padding: const EdgeInsets.only(
          bottom: 8.0,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            RaisedButton(
              color: Colors.blue,
              textColor: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: inLoading ? CircularProgressIndicator() : Text("Назад"),
              ),
              onPressed: prevPage,
            ),
            RaisedButton(
              color: Colors.blue,
              textColor: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: inLoading
                    ? CircularProgressIndicator()
                    : Text(isLast ? 'Отправить' : "Дальше"),
              ),
              onPressed: inLoading ? null : isLast ? startSubmit : nextPage,
            ),
          ],
        ),
      )
    ]);

    return Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch, children: items));
  }

  PageController _pageController = PageController();

  getQr() {
    setState(() {
      Widget qrIMage = QrImage(
        size: 200,
        data: RootService().user.model.counterType == 1
            ? RootService().kontragentService.buildPaymentData(
                RootService().user.model,
                RootService().user.model.electrokontragent,
                oneCount: double.parse(dayCount.text)/10 -
                    (lastBill == null ? 0 : int.parse(lastBill['dayCount'])))
            : RootService().kontragentService.buildPaymentData(
                  RootService().user.model,
                  RootService().user.model.electrokontragent,
                  dayCount: double.parse(dayCount.text)/10 -
                      (lastBill == null ? 0 : int.parse(lastBill['dayCount'])),
                  nightCount: double.parse(nightCount.text)/10 -
                      (lastBill == null
                          ? 0
                          : int.parse(lastBill['nightCount'])),
                ),
      );
      int sum = 0;
      int totalCount = 0;

      String purp = "";
      var kontragent = RootService().user.model.electrokontragent;
      if (RootService().user.model.counterType == 1) {
        var oneCount = int.parse(dayCount.text) -
            (lastBill == null ? 0 : int.parse(lastBill['dayCount']));
        if (oneCount != null) {
          totalCount = oneCount;
          sum = totalCount * kontragent.price['oneTariff'];
          purp += "T1 $oneCount ";
        }
      } else {
        var tdayCount = int.parse(dayCount.text) -
            (lastBill == null ? 0 : int.parse(lastBill['dayCount']));
        var tnightCount = int.parse(nightCount.text) -
            (lastBill == null ? 0 : int.parse(lastBill['nightCount']));
        if (tdayCount != null && tnightCount != null) {
          totalCount = tnightCount + tdayCount;
          sum = tnightCount * (kontragent.price['twoTariffNight']) +
              tdayCount * (kontragent.price['twoTariffDay']);
          purp += "T1 $tdayCount T2 $tnightCount";
        }
      }

      qr = Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          qrIMage,
          Text(
              '(расход ${totalCount/10} кВт), Ул. ${RootService().user.model.street.title}, д.${RootService().user.model.house}, Оплата электричества по договору №10177|'),
          Text("Сумма: ${sum / 1000}")
        ],
      );
    });
  }

  shareQr() async {
    var z = await saveQr();

    ShareExtend.share(z, 'image');
  }

  saveQr() async {
    ByteData image = await QrPainter(
      emptyColor: Colors.black,
      color: Colors.white,
      gapless: false,
      data: RootService().user.model.counterType == 1
          ? RootService().kontragentService.buildPaymentData(
              RootService().user.model,
              RootService().user.model.electrokontragent,
              oneCount: (int.parse(dayCount.text)/10) -
                  (lastBill == null ? 0 : int.parse(lastBill['dayCount'])))
          : RootService().kontragentService.buildPaymentData(
                RootService().user.model,
                RootService().user.model.electrokontragent,
                dayCount: int.parse(dayCount.text) -
                    (lastBill == null ? 0 : double.parse(lastBill['dayCount'])),
                nightCount: int.parse(nightCount.text) -
                    (lastBill == null ? 0 : double.parse(lastBill['nightCount'])),
              ),
      version: QrVersions.auto,
    ).toImageData(600.0);

    Directory path = await getApplicationDocumentsDirectory();
    Directory apppath = Directory("/storage/emulated/0/snt_delegate/");
    if (!apppath.existsSync()) {
      apppath.createSync();
    }
    String filePath = apppath.path +
        "${DateTime.now().millisecondsSinceEpoch.toString()}-${RootService().user.uid}-qrcode.png";
    File file = File(filePath);
    file.writeAsBytesSync(image.buffer.asUint8List());
    FirebaseStorage.instance.ref().getRoot().child("qr-kvit").putFile(file);
    print(file);
    return filePath;
  }

  Widget qr;

  Widget get lastBillDiff {
    if (lastBill != null && lastBill['creationTime'] != null) {
      int diff =
          lastBill['creationTime'].toDate().difference(DateTime.now()).inDays;
      return Text(
        "Прошлые показания были " + diff.toString() + " дней назад",
        style: TextStyle(color: () {
          Color color = Colors.green;
          if (diff > 20) {
            color = Colors.red;
          }
          if (diff < 20 && diff > 1) {
            color = Colors.yellow;
          }
          return color;
        }()),
      );
    }

    return Container();
  }

  @override
  Widget build(BuildContext context) {
    if (!isDataLoaded) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return PageView(
      controller: _pageController,
      children: <Widget>[
        buildPage(children: [
          SingleChildScrollView(
            child: Column(
                children: RootService().user.model.counterType == 2
                    ? [
                        CounterDataWidget(
                          hint: "Показания дня",
                          controller: dayCount,
                        ),
                        CounterDataWidget(
                          hint: "Показания ночи",
                          controller: nightCount,
                        ),
                        lastBillDiff
                      ]
                    : [
                        CounterDataWidget(
                          hint: "Показания дня",
                          controller: dayCount,
                        ),
                        lastBillDiff
                      ]),
          ),
        ]),
        buildPage(children: [
          Flexible(
            flex: 12,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  qr == null
                      ? RaisedButton(
                          onPressed: getQr,
                          child: Text("Сгенерировать qr"),
                        )
                      : qr,
                  Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.save),
                        onPressed: saveQr,
                      ),
                      IconButton(
                        icon: Icon(Icons.share),
                        onPressed: shareQr,
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ]),
        buildPage(children: [
          Text('Если вы оплатили, пожалуйста, загрузите квитанцию банка'),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Оплачено ли'),
              Switch(
                value: isPayed,
                onChanged: doPayChange,
              )
            ],
          ),
          ImageChooser(
            title: "Фото квитанции",
            controller: checkPhoto,
          ),
        ]),
        buildPage(isLast: true, children: [
          Text(
              '''Экспериментальная функция \nМы просим Вас отправить фото счетчика с теми же показаниями что и в квитанции. \nЗа это начисляется вознаграждение 10 баллов.'''),
          ImageChooser(
            title: "Фото счетчика",
            controller: devicePhoto,
          ),
        ])
      ],
    );
  }
}
