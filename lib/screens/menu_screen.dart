import 'package:flutter/material.dart';
import 'package:snt_delegate/services/RootService.dart';
import 'package:snt_delegate/widgets/WDrawer.dart';

class MenuScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: WDrawer(),
      appBar: AppBar(
        title: Text('Мой профиль'),
      ),
      body: MenuScreenBody(),
    );
  }
}

class MenuScreenBody extends StatefulWidget {
  @override
  _MenuScreenBodyState createState() => _MenuScreenBodyState();
}

class _MenuScreenBodyState extends State<MenuScreenBody> {
  goToProfile() => Navigator.pushNamed(context, "/profile/");

  goToVoting() => Navigator.pushNamed(context, '/election/list/');
  goToReceipt() => Navigator.pushNamed(context, "/bills/create/");

  goToHistory() => Navigator.pushNamed(context, "/bills/history/");
  goToForeignReceipt() =>
      Navigator.pushNamed(context, '/bills/create/foreign/');
  goToInstruction() => Navigator.pushNamed(context, '/instruction/');
  goToNewRegister() => Navigator.pushNamed(context, '/auth/new/');

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 0),
      child: ListView(
        children: <Widget>[
          ListTile(
            onTap: goToProfile,
            title: Text('Ваши бонусы'),
            subtitle: Text(RootService().user.model.bonuses.toString()),
            leading: Icon(Icons.star),
            enabled: true,
            selected: true,
          ),
          ListTile(
            onTap: goToProfile,
            title: Text('Мой профиль'),
            leading: Icon(Icons.person),
            enabled: true,
            selected: true,
          ),
          Divider(),
          ListTile(
            onTap: goToVoting,
            title: Text('Голосование'),
            leading: Icon(Icons.check),
            enabled: true,
          ),
          Divider(),
          ListTile(
            onTap: goToReceipt,
            title: Text('Подача показаний'),
            leading: Icon(Icons.receipt),
            enabled: true,
          ),
          Divider(),
          ListTile(
            onTap: goToHistory,
            title: Text('История'),
            leading: Icon(Icons.history),
            enabled: true,
          ),
          // Divider(),
          // ListTile(
          //   onTap: goToForeignReceipt,
          //   title: Text('Показания за другого'),
          //   leading: Icon(Icons.receipt),
          //   enabled: true,
          // ),
          Divider(),
          ListTile(
            onTap: goToInstruction,
            title: Text('Инструкция'),
            leading: Icon(Icons.help),
            enabled: true,
          ),
          Divider(),
          RootService().user.model.isDelegate
              ? ListTile(
                  onTap: goToNewRegister,
                  title: Text('Новый пользователь'),
                  leading: Icon(Icons.person_add),
                  enabled: true,
                )
              : Container()
        ],
      ),
    );
  }
}
