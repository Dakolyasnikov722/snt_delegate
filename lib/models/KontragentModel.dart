class KontragentModel {
  String name;
  String bankName;
  String bic;
  String correspAcc;
  String kpp;
  String payeeInn;
  String personalAcc;
  Map<String, int> price;

  static KontragentModel fromMap(Map data) {
    data.forEach((k, v) {
      if (v == null) {
        print("$v == null");
      }
    });
    KontragentModel model = KontragentModel();
    model.name = data['Name'];
    model.bankName = data['BankName'];
    model.bic = data['BIC'];
    model.correspAcc = data['CorrespAcc'];
    model.kpp = data['KPP'];
    model.payeeInn = data['PayeeINN'];
    model.personalAcc = data['PersonalAcc'];
    model.price = Map();
    model.price['oneTariff'] = data['price']['oneTariff'];
    model.price['twoTariffNight'] = data['price']['twoTariffNight'];
    model.price['twoTariffDay'] = data['price']['twoTariffDay'];
    return model;
  }
}
