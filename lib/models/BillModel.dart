import 'package:cloud_firestore/cloud_firestore.dart';

class BillModel {
  String docId;
  String dayCount;
  String nightCount;
  bool isPayed;
  String billUserId;
  String billOwnerId;
  String billCreatorName;
  String billOwnerName;
  String devicePhoto;
  String checkPhoto;
  Timestamp creationTime;

  String payQrLink =
      "https://auditsnt.ru/qrcode/c98b2f7d7011b8099aa1f8da0e8bdcdf.png";

  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = {
      'dayCount': dayCount,
      'nightCount': nightCount,
      'isPayed': isPayed,
      'billCreatorId': billUserId,
      'billOwnerId': billOwnerId,
      'billOwnerName': billOwnerName,
      'billCreatorName': billCreatorName,
      'devicePhoto': devicePhoto,
      'checkPhoto': checkPhoto,
      'creationTime': creationTime,
    };
    return data;
  }

  static BillModel fromMap(Map<String, dynamic> data) {
    BillModel bill = BillModel();
    bill.dayCount = data['dayCount'];
    bill.nightCount = data['nightCount'];
    bill.isPayed = data['isPayed'];
    bill.billOwnerId = data['billOwnerId'];
    bill.billUserId = data['billUserId'];
    bill.billCreatorName = data['billCreatorName'];
    bill.billOwnerName = data['billOwnerName'];
    bill.devicePhoto = data['devicePhoto'];
    bill.checkPhoto = data['checkPhoto'];
    bill.creationTime = data['creationTime'];
    return bill;
  }

  save() async {
    return await Firestore.instance.collection('bills').add(toMap());
  }
}
