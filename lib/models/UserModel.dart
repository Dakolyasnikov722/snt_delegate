import 'package:snt_delegate/models/KontragentModel.dart';
import 'package:snt_delegate/models/SntModel.dart';
import 'package:snt_delegate/models/StreetModel.dart';
import 'package:snt_delegate/services/RootService.dart';

class UserModel {
  String uid;
  bool isDelegate = false;
  String delegate;

  // Поля заполняемые при регистрации
  SntModel snt;
  KontragentModel electrokontragent;
  StreetModel street;
  String name;
  String lastname;
  String grandname;
  String house;
  String phone;
  String lastPayedCounterBeforeRegisterDay;
  String lastPayedCounterBeforeRegisterNight;
  String currentCounterDay;
  String currentCounterNight;

  String counter = "Двухтарифный";
  int counterType = 2;

//   модель счетчика
  String counterModel;
  // серийный номер прибора учета
  String counterSN;
  // дата установки прибора учета
  String counterSetupDate;
  // вводной автомат, А
  String uzoAmpers;

  // количество постоянно проживающих
  String humanCount;

  // лицевой счет МУК
  String mukAccountId;

  // пароль МУК
  String mukPassword;

  bool agreeWithOfert = true;
  int bonuses = 0;

  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = Map();
    data['name'] = name == null ? "Не выбрано" : name;
    data['lastname'] = lastname == null ? "Не выбрано" : lastname;
    data['grandname'] = grandname == null ? "Не выбрано" : grandname;
    data['phone'] = phone == null ? "Не выбрано" : phone;
    data['street_index'] = street == null ? null : street.index;
    data['street'] = street == null ? "Не выбрано" : street.title;
    data['snt_index'] = snt == null ? null : snt.index;
    data['snt'] = snt == null ? "Не выбрано" : snt.title;
    data['feeder'] = street.feeder;
    data['kontragent'] = electrokontragent == null ? null : electrokontragent.name;
    data['transformator'] = street.transformator;
    data['is_delegate'] = isDelegate;
    data['counter'] = counter;
    data['house'] = house;
    data['counter_type'] = counterType;
    data['delegate'] = delegate;
    data['lastPayedCounterBeforeRegisterDay'] =
        lastPayedCounterBeforeRegisterDay;
    data['lastPayedCounterBeforeRegisterNight'] =
        lastPayedCounterBeforeRegisterNight;
    data['currentCounterDay'] = currentCounterDay;
    data['currentCounterNight'] = currentCounterNight;

    data['counterModel'] = counterModel;
    data['counterSN'] = counterSN;
    data['counterSetupDate'] = counterSetupDate;
    data['uzoAmpers'] = uzoAmpers;
    data['humanCount'] = humanCount;
    data['mukAccountId'] = mukAccountId;
    data['mukPassword'] = mukPassword;
    data['agreeWithOfert'] = agreeWithOfert;

    return data;
  }

  static UserModel fromMap(Map<dynamic, dynamic> data) {
    UserModel _user = UserModel();
    _user.name = data['name'];
    _user.lastname = data['lastname'];
    _user.grandname = data['grandname'];
    _user.phone = data['phone'];
    _user.street =
        RootService().streetStore.getByIdOrFirst(data['street_index']);
    _user.snt = RootService().sntStore.getByIdOrFirst(data['snt_index']);
    _user.electrokontragent = RootService()
        .kontragentService
        .getElectroKontragentByName(data['kontragent']);
    _user.house = data['house'];
    _user.isDelegate = data['is_delegate'];
    _user.counter = data['counter'];
    _user.counterType = data['counter_type'];
    _user.delegate = data['delegate'];
    _user.currentCounterNight = data['currentCounterNight'];
    _user.currentCounterDay = data['currentCounterDay'];
    _user.lastPayedCounterBeforeRegisterNight =
        data['lastPayedCounterBeforeRegisterNight'];
    _user.lastPayedCounterBeforeRegisterDay =
        data['lastPayedCounterBeforeRegisterDay'];

    _user.counterModel = data['counterModel'];
    _user.counterSN = data['counterSN'];
    _user.counterSetupDate = data['counterSetupDate'];
    _user.uzoAmpers = data['uzoAmpers'];
    _user.humanCount = data['humanCount'];
    _user.mukAccountId = data['mukAccountId'];
    _user.mukPassword = data['mukPassword'];
    _user.bonuses = data['bonuses'] == null ? 0 : data['bonuses'];
    return _user;
  }
}
