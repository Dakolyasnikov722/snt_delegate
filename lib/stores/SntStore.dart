import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:snt_delegate/models/SntModel.dart';

class SntStore {
  List<SntModel> snts;

  SntModel getByIdOrFirst(int index) {
    return index == null || snts.length <= index ? snts.first : snts[index];
  }

  Future init() async {
    var streetsDoc =
        await Firestore.instance.collection('settings').document('snt').get();
    List rawStreets = streetsDoc.data['profile'];
    snts = List.generate(rawStreets.length, (i) {
      Map street = rawStreets[i];
      return SntModel(index: i, title: street['title']);
    });
  }
}
