import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:snt_delegate/models/StreetModel.dart';

class StreetStore {
  List<StreetModel> streets;

  StreetModel getByIdOrFirst(int index) {
    return index == null || streets.length <= index
        ? streets.first
        : streets[index];
  }

  Future init() async {
    var streetsDoc = await Firestore.instance
        .collection('settings')
        .document('streets')
        .get();
    List rawStreets = streetsDoc.data['profile'];
    streets = List.generate(rawStreets.length, (i) {
      Map street = rawStreets[i];
      return StreetModel(
          index: i,
          feeder: street['feeder'],
          title: street['title'],
          transformator: street['transformator']);
    });
  }
}
