import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:snt_delegate/models/SntModel.dart';
import 'package:snt_delegate/models/StreetModel.dart';
import 'package:snt_delegate/services/RootService.dart';
import 'package:snt_delegate/widgets/CounterChooser.dart';
import 'package:snt_delegate/widgets/CounterDataWidget.dart';
import 'package:snt_delegate/widgets/SntChooser.dart';
import 'package:snt_delegate/widgets/StreetChooser.dart';

class NewUserBody extends StatefulWidget {
  final Function(Map<String, dynamic> _cntrls, StreetModel street,
      SntModel snt) onSubmit;

  const NewUserBody({Key key, @required this.onSubmit}) : super(key: key);
  @override
  _NewUserBodyState createState() => _NewUserBodyState();
}

class _NewUserBodyState extends State<NewUserBody> {
  PageController _pageController = PageController();
  List<Map> counters;
  List<StreetModel> streets;
  List<SntModel> snts;
  StreetModel _userStreet;
  SntModel _userSnt;

  Map<String, dynamic> _cntrls = {
    "username": TextEditingController(),
    "lastPayedCounterBeforeRegisterDay": CounterDataController(CounterData(count: 6,)),
    "lastPayedCounterBeforeRegisterNight": CounterDataController(CounterData(count: 6,)),
    "currentCounterDay": CounterDataController(CounterData(count: 6,)),
    "currentCounterNight": CounterDataController(CounterData(count: 6,)),
    "counter": TextEditingController(),
    "counter_type": TextEditingController(),
  };

  doSubmit() {
    widget.onSubmit(_cntrls, _userStreet, _userSnt);
  }

  doNext() {
    _pageController.nextPage(
        curve: Curves.bounceInOut, duration: Duration(milliseconds: 300));
  }

  Widget buildPage({List<Widget> children, isLast = false}) {
    List<Widget> items = List();
    items.addAll(children);
    items.add(Padding(
      padding: const EdgeInsets.only(top: 15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          RaisedButton.icon(
            textColor: Colors.white,
            onPressed: isLast ? doSubmit : doNext,
            icon: Icon(
              Icons.chevron_right,
              color: Colors.white,
            ),
            label: isLast ? Text("Готово") : Text('Далее'),
            color: Colors.green,
          ),
        ],
      ),
    ));
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: items,
    );
  }

  chooseCounter() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (ctx) => CounterChooserScreen(
                  onChanged: onChangeCounter,
                  counters: counters,
                )));
  }

  onChangeCounter(Map counter) {
    _cntrls['counter'].text = counter['title'];
    _cntrls['counter_type'].text = counter['value'].toString();
  }

  chooseStreet() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (ctx) => StreetChooserScreen(
                  onChanged: onChangeStreet,
                  streets: streets,
                )));
  }

  onChangeStreet(StreetModel street) {
    setState(() {
      _userStreet = street;
    });
  }

  chooseSnt() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (ctx) => SntChooserScreen(
                  onChanged: onChangeSnt,
                  snt: snts,
                )));
  }

  onChangeSnt(SntModel snt) {
    setState(() {
      _userSnt = snt;
    });
  }

  loadData() async {
    snts = RootService().sntStore.snts;
    streets = RootService().streetStore.streets;
    var rawCounters = await Firestore.instance
        .collection('settings')
        .document('countertypes')
        .get();
    counters = List<Map>.generate(rawCounters.data.length, (i) {
      return rawCounters.data[rawCounters.data.keys.toList()[i]];
    });
  }

  @override
  void initState() {
    super.initState();
    loadData().then((v) {
      setState(() {
        isLoading = false;
      });
    });
  }

  bool isLoading = true;

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      alignment: Alignment.center,
      child: PageView(
        controller: _pageController,
        children: <Widget>[
          buildPage(children: [
            CheckboxListTile(
              onChanged: (v) {},
              value: true,
              title: Text(
                  'Подтверждаю свое согласие на обработку ваших персональных данных согласно ФЗ-152'),
            ),
            CheckboxListTile(
              onChanged: (v) {},
              value: true,
              title: Text(
                  'Подтверждаю свое согласие с договором оферты, использовать приложение в том виде "как есть" '),
            )
          ]),
          buildPage(children: [
            TextField(
              controller: _cntrls["username"],
              decoration: InputDecoration(
                  labelText: "Придумайте ваше имя",
                  hintText: "Имя",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(4),
                      borderSide: BorderSide(width: 1))),
            ),
          ]),
          buildPage(children: [
            TextField(
              enabled: false,
              controller: _cntrls['counter'],
            ),
            RaisedButton(
              child: Text('Выбрать тип счетчик'),
              onPressed: chooseCounter,
            ),
          ]),
          buildPage(children: [
            Text(_userSnt == null ? "Не выбрано" : _userSnt.title),
            Padding(
              padding: EdgeInsets.only(top: 15),
            ),
            RaisedButton(
              child: Text('Выбрать СНТ '),
              onPressed: chooseSnt,
            ),
            Padding(
              padding: EdgeInsets.only(top: 15),
            ),
            Text(_userStreet == null ? "Не выбрано" : _userStreet.title),
            Padding(
              padding: EdgeInsets.only(top: 15),
            ),
            RaisedButton(
              child: Text('Выбрать улицу '),
              onPressed: chooseStreet,
            ),
          ]),
          _cntrls['counter_type'].text == "2"
              ? buildPage(children: [
                 CounterDataWidget(
                    hint: "Последние оплаченные показатели дня",
                    controller:_cntrls["lastPayedCounterBeforeRegisterDay"] ,
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 15),
                  ),
                   CounterDataWidget(
                    hint: "Последние оплаченные показатели ночи",
                    controller:_cntrls["lastPayedCounterBeforeRegisterNight"] ,
                  ),
                ])
              : buildPage(children: [
                  CounterDataWidget(
                    hint: "Последние оплаченные показатели дня",
                    controller:_cntrls["lastPayedCounterBeforeRegisterDay"] ,
                  ),
                ]),
          _cntrls['counter_type'].text == "2"
              ? buildPage(isLast: true, children: [
                  CounterDataWidget(
                    hint: "Текущие показатели дня",
                    controller:_cntrls["currentCounterDay"] ,
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 15),
                  ),
                   CounterDataWidget(
                    hint: "Текущие показатели ночи",
                    controller:_cntrls["currentCounterNight"] ,
                  ),
                ])
              : buildPage(isLast: true, children: [
                    CounterDataWidget(
                    hint: "Текущие показатели дня",
                    controller:_cntrls["currentCounterDay"] ,
                  ),
                ])
        ],
      ),
    );
  }
}
