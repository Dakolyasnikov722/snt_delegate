import 'package:flutter/material.dart';

class CounterData {
  List<int> digits;
  CounterData({@required count, String value}) {
    if (value == null) {
      digits = List.from(List.generate(count, (i) => 0));
    } else {
      if (value.length == count) {
        digits = List.from(List.generate(count, (i) => int.parse(value[i])));
      } else {
        throw Exception("count != value.length");
      }
    }
  }
}

class CounterDataController extends ValueNotifier<CounterData> {
  CounterDataController(CounterData value) : super(value);
  get text => value.digits.join("");
  set text(String s) {
    value = CounterData(count: s.length, value: s);
  }

  String getDigitByIndex(i) =>
      i < value.digits.length ? value.digits[i].toString() : null;
  incrByIndex(i) {
    if (i < value.digits.length && value.digits[i] < 9) {
      value.digits[i]++;
      notifyListeners();
    }
  }

  decrByIndex(i) {
    if (i < value.digits.length && value.digits[i] > 0) {
      value.digits[i]--;
      notifyListeners();
    }
  }
}

class CounterDataWidget extends StatefulWidget {
  final String hint;
  final CounterDataController controller;

  const CounterDataWidget({Key key, this.controller, @required this.hint})
      : super(key: key);

  @override
  _CounterDataWidgetState createState() => _CounterDataWidgetState();
}

class _CounterDataWidgetState extends State<CounterDataWidget> {
  @override
  void initState() {
    super.initState();
    widget.controller.addListener(update);
  }

  @override
  void dispose() {
    widget.controller.removeListener(update);
    super.dispose();
  }

  update() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            widget.hint,
            style: TextStyle(fontSize: 20),
          ),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children:
                  List.generate(widget.controller.value.digits.length, (i) {
                int digit = widget.controller.value.digits[i];
                return DataCounterCell(widget.controller, i);
              })),
        ],
      ),
    );
  }
}

class DataCounterCell extends StatelessWidget {
  final CounterDataController controller;
  final int index;

  const DataCounterCell(this.controller, this.index, {Key key})
      : super(key: key);

  incr() => controller.incrByIndex(index);
  decr() => controller.decrByIndex(index);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: index == 5 ? Colors.red : Colors.white)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            onPressed: incr,
            icon: Icon(Icons.add),
          ),
          Text(
            controller.getDigitByIndex(index),
            style: TextStyle(fontSize: 28),
            textAlign: TextAlign.center,
          ),
          IconButton(
            onPressed: decr,
            icon: Icon(Icons.remove),
          ),
        ],
      ),
    );
  }
}
