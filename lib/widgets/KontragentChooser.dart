import 'package:flutter/material.dart';
import 'package:snt_delegate/models/KontragentModel.dart';


class KontragetChooserScreen extends StatelessWidget {
  final Function onChanged;
  final List<KontragentModel> list;

  const KontragetChooserScreen(
      {Key key, @required this.onChanged, @required this.list})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Выбор СНТ"),
      ),
      body: KontragetChooserScreenBody(
        onChanged: onChanged,
        list: list,
      ),
    );
  }
}

class KontragetChooserScreenBody extends StatelessWidget {
  final List<KontragentModel> list;
  final Function onChanged;

  const KontragetChooserScreenBody(
      {Key key, @required this.list, @required this.onChanged})
      : super(key: key);
      
  onChange(BuildContext context, KontragentModel street) {
    onChanged(street);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: List.generate(list.length, (i) {
        return ListTile(
          title: Text(list[i].name),
          onTap: () => onChange(context, list[i]),
        );
      }),
    );
  }
}
