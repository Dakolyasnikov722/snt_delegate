import 'package:flutter/material.dart';
import 'package:snt_delegate/models/SntModel.dart';

class SntChooserScreen extends StatelessWidget {
  final Function onChanged;
  final List<SntModel> snt;

  const SntChooserScreen(
      {Key key, @required this.onChanged, @required this.snt})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Выбор СНТ"),
      ),
      body: SntChooserScreenBody(
        onChanged: onChanged,
        snt: snt,
      ),
    );
  }
}

class SntChooserScreenBody extends StatelessWidget {
  final List<SntModel> snt;
  final Function onChanged;

  const SntChooserScreenBody(
      {Key key, @required this.snt, @required this.onChanged})
      : super(key: key);
      
  onChange(BuildContext context, SntModel street) {
    onChanged(street);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: List.generate(snt.length, (i) {
        return ListTile(
          title: Text(snt[i].title),
          onTap: () => onChange(context, snt[i]),
        );
      }),
    );
  }
}
