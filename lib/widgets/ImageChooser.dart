import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImageChooserController extends ValueNotifier<File> {
  ImageChooserController(File value) : super(value);
  bool inLoading = false;
  loadDone() => inLoading = false;
  loadStart() => inLoading = true;
}

class ImageChooser extends StatefulWidget {
  final String title;
  final ImageChooserController controller;

  const ImageChooser({Key key, @required this.controller, this.title})
      : super(key: key);
  @override
  _ImageChooserState createState() => _ImageChooserState();
}

class _ImageChooserState extends State<ImageChooser> {
  clearImage() {
    setState(() {
      widget.controller.value = null;
    });
  }

  chooseImage() async {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
              content: Text('Выберите источник фотографии'),
              actions: <Widget>[
                FlatButton.icon(
                  icon: Icon(Icons.camera),
                  label: Text('С камеры'),
                  onPressed: chooseFromCamera,
                ),
                FlatButton.icon(
                  icon: Icon(Icons.filter),
                  label: Text('Из галереи'),
                  onPressed: chooseFromGallery,
                )
              ],
            ));
  }

  chooseFromGallery() {
    Navigator.pop(context);
    ImagePicker.pickImage(source: ImageSource.gallery).then(update);
  }

  chooseFromCamera() {
    Navigator.pop(context);
    ImagePicker.pickImage(source: ImageSource.camera).then(update);
  }

  update(File v) {
    setState(() {
      widget.controller.value = v;
    });
  }

  Widget buildPreview() {
    if (widget.controller.value == null) {
      return Container();
    }
    return GestureDetector(
      onTap: clearImage,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Image.file(
            widget.controller.value,
            height: 60,
          ),
          Icon(
            Icons.close,
            color: Colors.red,
            size:60
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      alignment: Alignment.center,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          RaisedButton.icon(
            onPressed: chooseImage,
            icon: Icon(Icons.file_upload),
            label: Text(widget.title == null ? 'Загрузить' : widget.title),
          ),
          buildPreview()
        ],
      ),
    );
  }
}
