import 'package:flutter/material.dart';

class CounterChooserScreen extends StatelessWidget {
  final Function onChanged;
  final List<Map> counters;

  const CounterChooserScreen(
      {Key key, @required this.onChanged, @required this.counters})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Выбор типа счетчика"),
      ),
      body: CounterChooserScreenBody(
        onChanged: onChanged,
        counters: counters,
      ),
    );
  }
}

class CounterChooserScreenBody extends StatelessWidget {
  final List<Map> counters;
  final Function onChanged;

  const CounterChooserScreenBody(
      {Key key, @required this.counters, @required this.onChanged})
      : super(key: key);
      
  onChange(BuildContext context, Map street) {
    onChanged(street);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    // return Text(counters.toString());
    return ListView(
      children: List.generate(counters.length, (i) {
        return ListTile(
          title: Text(counters[i]['title']),
          onTap: () => onChange(context, counters[i]),
        );
      }),
    );
  }
}
