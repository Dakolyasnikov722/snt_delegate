import 'package:flutter/material.dart';

class UserRating extends StatelessWidget {
  final List<String> rating;

  const UserRating({Key key, @required this.rating}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: List.generate(rating.length, (i) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 3, vertical: 5),
          margin: EdgeInsets.symmetric(horizontal: 3, vertical: 5),
          color: Colors.yellow[200],
          child: Text(rating[i]),
        );
      }),
    );
  }
}
