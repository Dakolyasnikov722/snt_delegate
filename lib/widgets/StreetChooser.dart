import 'package:flutter/material.dart';
import 'package:snt_delegate/models/StreetModel.dart';

class StreetChooserScreen extends StatelessWidget {
  final Function onChanged;
  final List<StreetModel> streets;

  const StreetChooserScreen(
      {Key key, @required this.onChanged, @required this.streets})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Выбор улицы"),
      ),
      body: StreetChooserScreenBody(
        onChanged: onChanged,
        streets: streets,
      ),
    );
  }
}

class StreetChooserScreenBody extends StatelessWidget {
  final List<StreetModel> streets;
  final Function onChanged;

  const StreetChooserScreenBody(
      {Key key, @required this.streets, @required this.onChanged})
      : super(key: key);
      
  onChange(BuildContext context, StreetModel street) {
    onChanged(street);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: List.generate(streets.length, (i) {
        return ListTile(
          title: Text(streets[i].title),
          onTap: () => onChange(context, streets[i]),
        );
      }),
    );
  }
}
