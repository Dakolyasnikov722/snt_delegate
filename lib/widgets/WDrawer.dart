import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:snt_delegate/services/RootService.dart';

class WDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(child: WDrawerBody());
  }
}

class WDrawerBody extends StatefulWidget {
  @override
  _WdrawerBodyState createState() => _WdrawerBodyState();
}

class _WdrawerBodyState extends State<WDrawerBody> {
  Map voters;

  @override
  void initState() {
    super.initState();
    loadUser();
  }

  loadUser() async {
    var doc = await Firestore.instance
        .collection('election')
        .document(RootService().originalToken)
        .get();
    setState(() {
      if (doc.data != null &&
          doc['users'].runtimeType.toString() ==
              '_InternalLinkedHashMap<dynamic, dynamic>') {
        voters = doc['users'];
      } else {
        voters = Map();
      }
    });
  }

  onChangeUser(String uid) {
    RootService().user.authByFakeUser(uid).then((v) {
      Navigator.of(context).pushNamedAndRemoveUntil('/menu/', (r) => false);
    });
  }

  onReturnToOriginal() {
    RootService().user.resetToOriginalUser().then((v) {
      Navigator.of(context).pushNamedAndRemoveUntil('/menu/', (r) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Выберите пользователя',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                FlatButton(
                  textColor: Colors.white,
                  onPressed: onReturnToOriginal,
                  child: Text(
                    "Вернуться к своему профилю",
                    textAlign: TextAlign.start,
                  ),
                )
              ],
            ),
            decoration: BoxDecoration(color: Colors.blue),
          ),
          Flexible(
            child: voters == null
                ? Center(child: CircularProgressIndicator())
                : ListView(
                    children: List.generate(voters.keys.length, (i) {
                    var voter = voters[voters.keys.toList()[i]];
                    return ListTile(
                      selected: voter['uid'] == RootService().user.uid,
                      title: Text(voter['name']),
                      trailing: Icon(voter['uid'] == RootService().user.uid
                          ? Icons.check
                          : Icons.chevron_right),
                      onTap: () {
                        if (voter['uid'] != RootService().user.uid) {
                          onChangeUser(voter['uid']);
                        }
                      },
                    );
                  })),
          )
        ],
      ),
    );
  }
}
