import 'package:snt_delegate/screens/auth.dart';
import 'package:snt_delegate/screens/auth_failed.dart';
import 'package:snt_delegate/screens/auth_reg.dart';
import 'package:snt_delegate/screens/bills_create.dart';
import 'package:snt_delegate/screens/bills_create_foreign.dart';
import 'package:snt_delegate/screens/bills_history.dart';
import 'package:snt_delegate/screens/election.dart';
import 'package:snt_delegate/screens/instruction_screen.dart';

import 'package:snt_delegate/screens/menu_screen.dart';
import 'package:snt_delegate/screens/new_user.dart';
import 'package:snt_delegate/screens/profile.dart';

import 'package:flutter/material.dart';

void main() => runApp(EnergyApp());

class EnergyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: AuthScreen(),
      routes: {
        "/auth/failed/": (ctx) => AuthFailedScreen(),
        "/auth/reg/": (ctx) => AuthRegScreen(),
        "/bills/create/": (ctx) => BillCreateScreen(),
        "/bills/create/foreign/": (ctx) => BillCreateForeignScreen(),
        "/bills/history/": (ctx) => BillHistoryScreen(),
        "/election/list/": (ctx) => ElectionScreen(),
        "/menu/": (ctx) => MenuScreen(),
        "/profile/": (ctx) => ProfileScreen(),
        "/instruction/": (ctx) => InstructionScreen(),
        "/auth/new/": (ctx) => NewUserScreen()
      },
    );
  }
}
