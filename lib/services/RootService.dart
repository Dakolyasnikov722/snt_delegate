import 'package:snt_delegate/services/BillService.dart';
import 'package:snt_delegate/services/KontragentService.dart';
import 'package:snt_delegate/services/UserService.dart';
import 'package:snt_delegate/stores/SntStore.dart';
import 'package:snt_delegate/stores/StreetStore.dart';

class RootService {
  static final RootService _singletone = RootService._privateConstructor();
  RootService._privateConstructor();
  factory RootService() {
    return _singletone;
  }
  String originalToken;
  UserService user;
  BillService bills;
  KontragentService kontragentService;
  init() async {
    await initStores();
    kontragentService = KontragentService();
    await kontragentService.init();
    user = UserService();
    bills = BillService();
   
    await user.init();
  }

  StreetStore streetStore = StreetStore();
  SntStore sntStore = SntStore();



  initStores() async {
    await streetStore.init();
    await sntStore.init();
  }
}
