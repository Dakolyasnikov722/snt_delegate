import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:snt_delegate/models/UserModel.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:snt_delegate/services/RootService.dart';

class UserService {
  GoogleSignIn googleSignIn;
  FirebaseAuth auth;
  FirebaseUser fireUser;
  UserModel model;

  String _fakeUid;

  init() async {
    googleSignIn = GoogleSignIn();
    auth = FirebaseAuth.instance;
    model = UserModel();
    return true;
  }

  fillUser() async {
    var data = await Firestore.instance.collection("users").document(uid).get();

    model = UserModel.fromMap(data.data);
  }

  String get uid => _fakeUid == null ? fireUser.uid : _fakeUid;

  update() async {
    await delegationStatusChange();
    return await Firestore.instance
        .collection('users')
        .document(uid)
        .updateData(model.toMap());
  }

  chooseMyDelegate(String delegateId, {bool choice}) async {
    var rawD = await Firestore.instance
        .collection('election')
        .document(delegateId)
        .get();
    var delegatElectionModel = rawD;
    Map myVote = {"name": model.name, "uid": uid};
    Map selectedUsers = Map();
    if (delegatElectionModel.data['users'] != null) {
      selectedUsers.addAll(delegatElectionModel.data['users'] as Map);
    }
    model.delegate = choice ? delegateId : null;
    await update();
    selectedUsers.keys.contains(uid)
        ? selectedUsers.remove(uid)
        : selectedUsers[uid] = myVote;
    delegatElectionModel.reference.updateData({'users': selectedUsers});
  }

  chooseCreatorAsDelegate(
      String foreignUid, String delegateId, String foreignName) async {
    var rawD = await Firestore.instance
        .collection('election')
        .document(delegateId)
        .get();
    var delegatElectionModel = rawD;
    Map myVote = {"name": foreignName, "uid": foreignUid};
    Map selectedUsers = Map();
    if (delegatElectionModel.data != null &&
        delegatElectionModel.data['users'] != null &&
        delegatElectionModel.data['users'].runtimeType.toString() !=
            "List<dynamic>") {
      selectedUsers.addAll(delegatElectionModel.data['users'] as Map);
    }

    model.delegate = delegateId;

    selectedUsers.keys.contains(uid)
        ? selectedUsers.remove(uid)
        : selectedUsers[foreignUid] = myVote;
    delegatElectionModel.reference.updateData({'users': selectedUsers});
  }

  delegationStatusChange() async {
    Map<String, dynamic> data = Map();
    data['active'] = model.isDelegate;
    data['name'] = model.name;
    data['street_index'] = model.street.index;

    var election =
        await Firestore.instance.collection('election').document(uid).get();
    // Есть ли в базе информация о состоянии голосования о данном пользователе
    if (election.exists) {
      // Если за этого человека уже голосовали, то сохраняем старые голоса
      data['users'] = election.data['users'];
    } else {
      // Если голосов еще нет, то отправляем в базу пустую модель списка
      data['users'] = List<String>();
    }

    Firestore.instance
        .collection('election')
        .document(uid)
        .setData(data, merge: true);
  }

  create() async {
    Map<String, dynamic> data = model.toMap();
    return await Firestore.instance
        .collection("users")
        .document(uid)
        .setData(data);
  }

  regForeignUser(String foreignUid, UserModel foreignUser) async {
    Map<String, dynamic> data = foreignUser.toMap();
    await Firestore.instance
        .collection("users")
        .document(foreignUid)
        .setData(data);
    await chooseCreatorAsDelegate(foreignUid, uid, foreignUser.name);
    await RootService().user.authByFakeUser(foreignUid);
  }

  authByFakeUser(String tokenuid) async {
    _fakeUid = tokenuid;
    await fillUser();
  }

  resetToOriginalUser() async {
    _fakeUid = null;
    await fillUser();
  }

  tryAuth(context) async {
    GoogleSignInAccount googleUser = await googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    AuthCredential credentials = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
    AuthResult _authResult = await auth.signInWithCredential(credentials);
    fireUser = _authResult.user;

    if (_authResult.user == null) {
      Navigator.of(context).pushReplacementNamed('/auth/failed/');
    } else {
      Firestore.instance
          .collection('users')
          .document(_authResult.user.uid)
          .get()
          .then((ds) async {
        if (ds.data == null) {
          Navigator.of(context).pushReplacementNamed('/auth/reg/');
        } else {
          await fillUser();
          RootService().originalToken = fireUser.uid;
          Navigator.of(context).pushReplacementNamed('/menu/');
        }
      });
    }
  }
}
