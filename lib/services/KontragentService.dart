import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:snt_delegate/models/KontragentModel.dart';
import 'package:snt_delegate/models/UserModel.dart';

class KontragentService {
  List<KontragentModel> electricity;

  init() async {
    electricity = List<KontragentModel>();
    await loadKontragents();
  }

  KontragentModel getElectroKontragentByName(String name) {
    KontragentModel result;
    for (KontragentModel k in electricity) {
      if (k.name == name) {
        result = k;
        break;
      }
    }
    return result;
  }

  loadKontragents() async {
    DocumentSnapshot kontragentsDoc = await Firestore.instance
        .collection('settings')
        .document('kontragents')
        .get();
    Map<String, dynamic> kontragents = kontragentsDoc.data;
    if (kontragents.containsKey('electricity')) {
      kontragents['electricity'].forEach((v) {
        electricity.add(KontragentModel.fromMap(v));
      });
    }
  }

  String buildPaymentData(UserModel user, KontragentModel kontragent, {double oneCount , double dayCount , double nightCount }){
    double sum = 0;
    double totalCount = 0;

    String purp = "";


    if(oneCount != null){
      totalCount = oneCount;
      sum = totalCount*kontragent.price['oneTariff'];
      purp += "T1 $oneCount ";

    } 
    if (dayCount != null && nightCount != null){
      totalCount = nightCount + dayCount;
      sum = (nightCount*kontragent.price['twoTariffNight']);
      print(kontragent.price['twoTariffDay']);
      sum += (dayCount*kontragent.price['twoTariffDay']) ;
      purp += "T1 $dayCount T2 $nightCount";
    }
    
    
    String result ="";
    result += 'ST00012|';
    result +='Name=${kontragent.name}|';
    result +='PersonalAcc=${kontragent.personalAcc}|';
    result +='BankName=${kontragent.bankName}|';
    result +='BIC=${kontragent.bic}|';
    result +='CorrespAcc=${kontragent.correspAcc}|'; 
    result +='KPP=${kontragent.kpp}|';
    result +='PayeeINN=${kontragent.payeeInn}|';
    result +='lastName=${user.name}|';
    result +='payerAddress=Ул. ${user.street.title}, д.${user.house}|';
    result +='Purpose= $purp (расход ${totalCount} кВт), Ул. ${user.street.title}, д.${user.house}, Оплата электричества по договору №10177|';
    result +='Sum=${sum}';
    
    return result;
  }
}
