import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:snt_delegate/models/BillModel.dart';
import 'package:snt_delegate/services/RootService.dart';

class BillService {
  Future<List<BillModel>> getHistory() async {
    List<BillModel> bills = List();
    var result = await Firestore.instance
        .collection('bills')
        .where('billOwnerId', isEqualTo: RootService().user.uid).orderBy('creationTime', descending: true)
        .getDocuments();
    result.documents.forEach((doc) {
      doc.data['docId'] = doc.documentID;
      bills.add(BillModel.fromMap(doc.data));
    });
    return bills;
  }
}
